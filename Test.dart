import 'dart:io';

import 'Frigg.dart';
import 'King.dart';
import 'Nemesis.dart';
import 'Tsubasa.dart';

void main(List<String> args) {
  var tsubasa = Tsubasa();
  tsubasa.setHP(1000);
  print("ค่าพลังของ ${tsubasa.getName()} คือ ${tsubasa.getHP()}");
  var king = King();
  king.setDEF(300);
  print("ค่าป้องกันของ ${king.getName()} คือ ${king.getDEF()}");
  var nemesis = Nemesis();
  print("ชื่อ  ${nemesis.getName()}");
  print("อาชีพของ ${nemesis.getName()} คือ ${nemesis.getNameOccupation()}");
  print("ธาตุของ ${nemesis.getName()} คือ ${nemesis.getType()}");
  var frigg = Frigg();
  print("ค่าโจมตีของ ${frigg.getName()} คือ ${frigg.getATK()}");
  frigg.setSPD(200);
  print("ความเร็วของ ${frigg.getName()} คือ ${frigg.getSPD()}");
}

