class Blade {
  String? nameOccupation = "blade";
  String? weapon = "double blade";
  String? rank = "SSR";
  int? ATK = 160;

  String? getNameOccupation() {
    return nameOccupation;
  }

  String? getWeapon() {
    return weapon;
  }

  String? getRank() {
    return rank;
  }

  int? getATK() {
    return ATK;
  }
}