class Archer {
  String? nameOccupation = "archer";
  String? weapon = "archer arrow";
  String? rank = "SSR";
  int? ATK = 150;

  String? getNameOccupation() {
    return nameOccupation;
  }

  String? getWeapon() {
    return weapon;
  }

  String? getRank() {
    return rank;
  }

  int? getATK() {
    return ATK;
  }
}
