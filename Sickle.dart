class Sickle {
  String? nameOccupation = "sickle";
  String? weapon = "sickle";
  String? rank = "SSR";
  int? ATK = 170;

  String? getNameOccupation() {
    return nameOccupation;
  }

  String? getWeapon() {
    return weapon;
  }

  String? getRank() {
    return rank;
  }

  int? getATK() {
    return ATK;
  }
}