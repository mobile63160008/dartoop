import 'dart:io';

import 'package:extend/extend.dart' as extend;

abstract class Agent {
  String? name;
  int? hp;
  int? def;
  int? spd;
  String? type;

  String? getName() {
    return name;
  }

  int? getHP() {
    return hp;
  }

  int? getDEF() {
    return def;
  }

  int? getSPD() {
    return spd;
  }

  String? getType() {
    return type;
  }

  void setHP(int hp) {
    this.hp = hp;
  }

  void setDEF(int def) {
    this.def = def;
  }

  void setSPD(int spd) {
    this.spd = spd;
  }
}
