class Gunslinger {
  String? nameOccupation = "gunslinger";
  String? weapon = "double gun";
  String? rank = "SSR";
  int? ATK = 180;

  String? getNameOccupation() {
    return nameOccupation;
  }

  String? getWeapon() {
    return weapon;
  }

  String? getRank() {
    return rank;
  }

  int? getATK() {
    return ATK;
  }
}